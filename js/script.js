let numberOfHuman = 100;

let probabilityOfIllness = 0.1;

let maxStep = 10;

let box = document.querySelector('.field');

let humans = [];

let boxWidth = box.getBoundingClientRect().width;

box.style.height = boxWidth + 'px';

createHumansArray();
drawField(true);
moveHumans();
setInterval(() => {
	moveHumans();
	drawField();
}, 1000);

function createHumansArray() {
	humans = [];
	for (let i = 0; i < numberOfHuman; i++) {
		let ho = { id: i };

		ho.x = Math.random() * boxWidth;
		ho.y = Math.random() * boxWidth;

		if (Math.random() < probabilityOfIllness) {
			ho.ill = true;
		}
		humans.push(ho);
	}
}

function drawField(create = false) {
	console.log('111');
	for (let i = 0; i < numberOfHuman; i++) {
		let ho = humans[i];
		let hh;
		if (create) {
			hh = document.createElement('div');
			hh.classList.add('human');
			hh.id = 'id' + i;
		} else hh = document.querySelector('#id' + i);

		hh.style.left = ho.x + 'px';
		hh.style.top = ho.y + 'px';
		if (ho.ill) {
			hh.classList.add('ill');
		}

		if (create) {
			box.append(hh);
		}
	}
}

function moveHumans() {
	for (let i = 0; i < numberOfHuman; i++) {
		let ho = humans[i];
		ho.x += Math.random() * maxStep - maxStep / 2;
		ho.y += Math.random() * maxStep - maxStep / 2;
	}
	for (let i = 0; i < numberOfHuman - 1; i++) {
		for (let j = i + 1; j < numberOfHuman; j++) {
			if (humans[i].ill || humans[j]) {
				let d = Math.sqrt((humans[i].x - humans[j].x) ** 2 + (humans[i].y - humans[j].y) ** 2);
                if(d<5){
                    humans[i].ill = true;
                    humans[j].ill = true;

                }
            }
            
		}
	}
}
